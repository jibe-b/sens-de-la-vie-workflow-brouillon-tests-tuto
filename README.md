# Sens de la vie (un exemple de workflow brouillon - tests - tuto)

## Exécuter le code en ligne

Pour exécuter le code en ligne et simuler le sens de la vie, il suffit de cliquer sur le bouton suivant :

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jibe-b%2Fsens-de-la-vie-workflow-brouillon-tests-tuto/dev?urlpath=lab%2Ftree%2Fsens-de-la-vie-tuto.ipynb)
