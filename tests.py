import sens_de_la_vie

def test_quel_est_le_sens_de_la_vie_retourne_un_entier():
    expected = type(int())
    actual = type(sens_de_la_vie.quel_est_le_sens_de_la_vie())
    
    assert(expected == actual)
    
